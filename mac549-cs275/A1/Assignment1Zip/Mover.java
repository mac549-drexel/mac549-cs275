import java.io.*;
import java.util.*;
import java.net.*;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;

import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.MoveFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.MoveFileOrFolder.MoveFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.MoveFileOrFolder.MoveFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileResultSet;
import com.temboo.Library.Dropbox.OAuth.*;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooSession;

public class Mover 
{
	public static void moveFile(
			TembooSession session, 
			String accessToken,
			String accessTokenSecret,
			String appKey,
			String appSecret,
			String fromPath,
			String toPath) throws Exception
	{
		MoveFileOrFolder moveFileOrFolderChoreo = new MoveFileOrFolder(session);

		// Get an InputSet object for the choreo
		MoveFileOrFolderInputSet moveFileOrFolderInputs = moveFileOrFolderChoreo.newInputSet();

		// Set inputs
		moveFileOrFolderInputs.set_AccessToken(accessToken);
		moveFileOrFolderInputs.set_AccessTokenSecret(accessTokenSecret);
		moveFileOrFolderInputs.set_AppKey(appKey);
		moveFileOrFolderInputs.set_AppSecret(appSecret);
		moveFileOrFolderInputs.set_FromPath(fromPath);
		moveFileOrFolderInputs.set_ToPath(toPath);
		// Execute Choreo
		MoveFileOrFolderResultSet moveFileOrFolderResults = moveFileOrFolderChoreo.execute(moveFileOrFolderInputs);
	}
	
	public static void deleteFile(
			TembooSession session, 
			String accessToken,
			String accessTokenSecret,
			String appKey,
			String appSecret,
			String path) throws Exception
	{
		//delete current __list file
		DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

		// Get an InputSet object for the choreo
		DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

		// Set inputs
		deleteFileOrFolderInputs.set_AccessToken(accessToken);
		deleteFileOrFolderInputs.set_AccessTokenSecret(accessTokenSecret);
		deleteFileOrFolderInputs.set_AppKey(appKey);
		deleteFileOrFolderInputs.set_AppSecret(appSecret);
		deleteFileOrFolderInputs.set_Path(path);
		// Execute Choreo
		DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
				
	}
	
	public static void uploadFile(TembooSession session, 
			String accessToken,
			String accessTokenSecret,
			String appKey,
			String appSecret,
			String fileContentsEncoded,
			String fileName,
			String folder) throws Exception
	{
		UploadFile uploadFileChoreo = new UploadFile(session);

		// Get an InputSet object for the choreo
		UploadFileInputSet uploadFileInputs = uploadFileChoreo.newInputSet();

		// Set inputs
		uploadFileInputs.set_AccessToken(accessToken);
		uploadFileInputs.set_AccessTokenSecret(accessTokenSecret);
		uploadFileInputs.set_AppKey(appKey);
		uploadFileInputs.set_AppSecret(appSecret);
		uploadFileInputs.set_FileContents(fileContentsEncoded);
		uploadFileInputs.set_FileName(fileName);
		uploadFileInputs.set_Folder(folder);
		
		// Execute Choreo
		UploadFileResultSet uploadFileResults = uploadFileChoreo.execute(uploadFileInputs);
	}
	
	public static void main(String[] args) throws Exception
	{
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession("markzakaev", "myFirstApp", "6c710778c8f74fc9abb8d10fa978ec38");

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);
		
		Scanner in = new Scanner(System.in);
		//Get Inputs
		System.out.println("Input app key:");
		String appKey = in.nextLine();
		System.out.println("Input app secret:");
		String appSecret = in.nextLine();
		
		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_DropboxAppSecret(appSecret);
		initializeOAuthInputs.set_DropboxAppKey(appKey);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		//get results
		String callbackID = initializeOAuthResults.get_CallbackID();
		String oauthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
		
		//direct user to authorize url
		System.out.println("Quick! Go here to authorize:\n(then hit 'enter' to finalize)\n" + initializeOAuthResults.get_AuthorizationURL());
		in.nextLine();
		//Finalize Oauth
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set credential to use for execution
		finalizeOAuthInputs.setCredential("DropboxOauthFinal");
		
		//Set Inputs
		finalizeOAuthInputs.set_CallbackID(callbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(oauthTokenSecret);
		finalizeOAuthInputs.set_DropboxAppKey(appKey);
		finalizeOAuthInputs.set_DropboxAppSecret(appSecret);
		
		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		String accessToken = finalizeOAuthResults.get_AccessToken();
		String accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		String userID = finalizeOAuthResults.get_UserID();
		
		//Get __list file
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_EncodeFileContent("false");
		getFileInputs.set_AccessToken(accessToken);
		getFileInputs.set_AppSecret(appSecret);
		getFileInputs.set_AccessTokenSecret(accessTokenSecret);
		getFileInputs.set_AppKey(appKey);
		getFileInputs.set_Path("move/__list.txt");

		// Execute Choreo
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		Scanner listFile = new Scanner(getFileResults.get_Response());
		//Get each line of the __list file and operate
		while(listFile.hasNextLine())
		{
			String[] currLine = listFile.nextLine().split(" ");
			String fromPath = "move/" + currLine[0];
			String toPath = currLine[1] + "/" + currLine[0];
			
			moveFile(session, accessToken, accessTokenSecret, 
					appKey, appSecret, fromPath, toPath);
			
		}
		//delete current __list file
		deleteFile(session, accessToken, accessTokenSecret, appKey, appSecret, "/move/__list.txt");
		
		//upload new, blank __list file 
		String fileContents = " "; //blank document
		String fileContentsEncoded = Base64.encodeBase64String(fileContents.getBytes("utf-8"));
		uploadFile(session, accessToken, accessTokenSecret, appKey, 
				appSecret, fileContentsEncoded, "__list.txt", "/move");
	}
}
