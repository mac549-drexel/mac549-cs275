import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
// Requires gson jars
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Wunderground {         
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// Get from http://www.wunderground.com/weather/api/
		String key;
		if(args.length < 1) {
			System.out.println("Enter key: ");
			
			Scanner in = new Scanner(System.in);
			key = in.nextLine();
		} else {
			key = args[0];
		}
		
		String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/19104.json";
		
		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		// Convert to a JSON object to print data
    	JsonParser jp = new JsonParser();
    	JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    	JsonObject rootobj = root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
    	
    	// Get some data elements and print them
    	//Geolookup
    	JsonObject loc = rootobj.get("location").getAsJsonObject();
    	String zip = loc.get("zip").getAsString();
    	String city = loc.get("city").getAsString();
    	String state = loc.get("state").getAsString();
    	
    	System.out.println("Zip: " + zip);
    	System.out.println("City: " + city);
    	System.out.println("State: " + state);
    	
    	//Hourly
    	sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/" + zip + ".json";
    	url = new URL(sURL);
		request = (HttpURLConnection) url.openConnection();
		request.connect();
		
		root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    	rootobj = root.getAsJsonObject();
    	//get an array of each hour object
    	JsonArray hourly = rootobj.getAsJsonArray("hourly_forecast");
    	//loop through the array
    	for (int i = 0; i < hourly.size(); i++)
    	{
    		//get the specific hour
    		JsonElement hour = hourly.get(i);
    		//get time and temp from the specific hour
    		JsonObject time = ((JsonObject) hour).get("FCTTIME").getAsJsonObject();
    		JsonObject temp = ((JsonObject) hour).get("temp").getAsJsonObject();
    		//Output date on one line, then conditions indented on the next
    		System.out.println("Time/Date: " + time.get("pretty").getAsString());
    		System.out.print("\tConditions: " + ((JsonObject) hour).get("condition").getAsString());
    		System.out.print(", Temp: " + temp.get("english").getAsString());
    		System.out.println(", Humidity: " + ((JsonObject) hour).get("humidity").getAsString());
    	}
	}

}