import java.io.*;
import java.util.*;
import java.net.*;

import com.google.gson.*;
import com.temboo.Library.Google.OAuth.*;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Google.Calendar.*;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsInputSet;
import com.temboo.Library.Google.Calendar.GetAllCalendars.GetAllCalendarsResultSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsInputSet;
import com.temboo.Library.Google.Calendar.GetAllEvents.GetAllEventsResultSet;
import com.temboo.core.TembooSession;


public class CalList 
{
	public static String oauth1(String client, String redirect)
	{
		return "https://accounts.google.com/o/oauth2/auth" +
				"?client_id=" + client +
				"&scope=https://www.googleapis.com/auth/calendar.readonly" +
				"&response_type=code" +
				"&redirect_uri=" + redirect;
	}
	
	public static String oauth2(String client, String redirect, String secret, String code)
	{
		return "https://www.googleapis.com/oauth2/v3/token" +
				"?code=" + code +
				"&client_id=" + client +
				"&client_secret=" + secret +
				"&redirect_uri=" + redirect +
				"&grant_type=authorization_code";
	}
	
	public static String executePost(String targetURL, String urlParameters)
    {
      URL url;
      HttpURLConnection connection = null;  
      try 
      {
        //Create connection
        url = new URL(targetURL);
        connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", 
             "application/x-www-form-urlencoded");
           
        connection.setRequestProperty("Content-Length", "" + 
                 Integer.toString(urlParameters.getBytes().length));
        connection.setRequestProperty("Content-Language", "en-US");  
           
        connection.setUseCaches (false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        //Send request
        DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream ());
        wr.writeBytes (urlParameters);
        wr.flush ();
        wr.close ();

        //Get Response   
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer(); 
        while((line = rd.readLine()) != null) 
        {
          response.append(line);
          response.append('\r');
        }
        rd.close();
        return response.toString();
      }
      catch (Exception e) 
      {
          e.printStackTrace();
          return null;
      }
      finally 
      {
          if(connection != null) 
          {
            connection.disconnect(); 
          }
      }
    }

	
	public static void main(String[] args) throws Exception
	{
		Scanner in = new Scanner(System.in);
		/*String clientID = "142273246102-le943jmp916o5ko2lact3umqmus0td9a.apps.googleusercontent.com";
		String redirect = "https://www.cs.drexel.edu/~wmm24/cs275_wi15/";
		String clientSecret = "_UOZ5KX2HH17EfQCMHOOlmbr";*/
		
		//Prompt user for inputs
		System.out.println("Input client id:");
		String clientID = in.nextLine();
		System.out.println("Input redirect uri:");
		String redirect = in.nextLine();
		System.out.println("Input client secret:");
		String clientSecret = in.nextLine();
		
		/*String oauthurl = oauth1(clientID, redirect);
		
		//Point to redirect URL
		System.out.println("Get the code from here:");
		System.out.println(oauthurl);
		
		System.out.println("Enter code:");
		String code = in.nextLine();
		
		//URL url = new URL(oauth2(clientID, redirect, clientSecret, code));
		//URL url = new URL("https://www.googleapis.com/oauth2/v3/token");
		//HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		String url = "https://accounts.google.com/o/oauth2/token";
		String urlParameters = "code=" + code +
				"&client_id=" + clientID +
				"&client_secret=" + clientSecret +
				"&redirect_uri=" + redirect +
				"&grant_type=authorization_code";
		
        String response = executePost(url, urlParameters);
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(response);
		JsonObject oauth_root = root.getAsJsonObject();
		String token = oauth_root.get("access_token").getAsString();
		
		String calendarRequest = "https://www.googleapis.com/calendar/v3/calendars/" + 
								"mark.andrew.cohen%40gmail.com/events" + 
								"?maxResults=10" +
								"&access_token=" + token;
		URL calReq = new URL(calendarRequest);
		HttpURLConnection calResponse = (HttpURLConnection) calReq.openConnection();
		calResponse.connect();
		
		JsonElement calListroot = jp.parse(new InputStreamReader((InputStream) calResponse.getContent()));
		JsonObject calList = calListroot.getAsJsonObject();
		JsonArray events = calList.get("items").getAsJsonArray();
		for(int i = events.size() - 1; (i >= events.size() - 5) || (i >= 0); i--)
		{
			JsonObject currEvent = events.get(i).getAsJsonObject();
			String title = currEvent.get("summary").getAsString();
			String time = currEvent.get("start").getAsJsonObject().get("dateTime").getAsString();
			System.out.println(title + " at " + time);
		}*/
		
		TembooSession session = new TembooSession("markzakaev", "myFirstApp", "6c710778c8f74fc9abb8d10fa978ec38");

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ClientID(clientID);
		initializeOAuthInputs.set_Scope("https://www.googleapis.com/auth/calendar.readonly");

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String authorizeURL = initializeOAuthResults.get_AuthorizationURL();
		System.out.println("Go to this URL to authorize:");
		System.out.println(authorizeURL);
		String callbackID = initializeOAuthResults.get_CallbackID();
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set credential to use for execution
		finalizeOAuthInputs.setCredential("Oauth2");

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callbackID);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		String accessToken = finalizeOAuthResults.get_AccessToken();
		
		GetAllCalendars getAllCalendarsChoreo = new GetAllCalendars(session);

		// Get an InputSet object for the choreo
		GetAllCalendarsInputSet getAllCalendarsInputs = getAllCalendarsChoreo.newInputSet();

		// Set inputs
		getAllCalendarsInputs.set_ClientSecret(clientSecret);
		getAllCalendarsInputs.set_AccessToken(accessToken);
		getAllCalendarsInputs.set_ClientID(clientID);

		// Execute Choreo
		GetAllCalendarsResultSet getAllCalendarsResults = getAllCalendarsChoreo.execute(getAllCalendarsInputs);
		String response = getAllCalendarsResults.get_Response();
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(response);
		String calendarID = root.getAsJsonObject().get("items").getAsJsonArray().
				get(0).getAsJsonObject().get("id").getAsString();
		
		GetAllEvents getAllEventsChoreo = new GetAllEvents(session);

		// Get an InputSet object for the choreo
		GetAllEventsInputSet getAllEventsInputs = getAllEventsChoreo.newInputSet();

		// Set inputs
		getAllEventsInputs.set_ClientSecret(clientSecret);
		getAllEventsInputs.set_CalendarID(calendarID);
		getAllEventsInputs.set_AccessToken(accessToken);
		getAllEventsInputs.set_ClientID(clientID);

		// Execute Choreo
		GetAllEventsResultSet getAllEventsResults = getAllEventsChoreo.execute(getAllEventsInputs);
		response = getAllEventsResults.get_Response();
		root = jp.parse(response);
		JsonObject eventResponse = root.getAsJsonObject();
		JsonArray events = eventResponse.get("items").getAsJsonArray();
		for(int i = 0; i < events.size(); i++)
		{
			JsonObject currEvent = events.get(i).getAsJsonObject();
			String title = currEvent.get("summary").getAsString();
			String time = "";
			if (currEvent.get("start").getAsJsonObject().has("dateTime"))
			{
				time = currEvent.get("start").getAsJsonObject().get("dateTime").getAsString();
			}
			else
			{
				time = currEvent.get("start").getAsJsonObject().get("date").getAsString();
			}
			System.out.println(title + " at " + time);
		}
	}
}
