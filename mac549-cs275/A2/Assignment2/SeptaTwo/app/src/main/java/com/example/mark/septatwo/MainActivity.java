package com.example.mark.septatwo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner from = (Spinner) findViewById(R.id.from);
        Spinner to = (Spinner) findViewById(R.id.to);
        List<String> stations = readCSV(getResources().openRawResource(R.raw.station_id_name));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, stations);

        from.setAdapter(adapter);
        to.setAdapter(adapter);
    }

    public void getTrains(View v)
    {
        Intent i = new Intent(getApplicationContext(), TrainList.class);

        Spinner spin = (Spinner) findViewById(R.id.from);
        String source = spin.getSelectedItem().toString();

        spin = (Spinner) findViewById(R.id.to);
        String dest = spin.getSelectedItem().toString();
        i.putExtra("source", source);
        i.putExtra("dest", dest);

        startActivity(i);
    }

    public List<String> readCSV(InputStream source)
    {
        List<String> data = new ArrayList();
        BufferedReader reader = new BufferedReader(new InputStreamReader(source));
        try
        {
            String line;
            while ((line = reader.readLine()) != null)
            {
                String row = line.split(",")[1];//get the station name
                data.add(row);
            }
            source.close();
        }
        catch (IOException e)
        {

        }

        return data;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
