package com.example.mark.septatwo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mark on 3/11/15.
 */
public class TrainListAdapter extends ArrayAdapter<Train>
{
    private Context cxt;
    private List<Train> trainList;

    public TrainListAdapter(Context _cxt, List<Train> _trainList)
    {
        super(_cxt, R.layout.list_item_layout, _trainList);
        cxt = _cxt;
        trainList = _trainList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;
        TrainHolder holder = new TrainHolder();

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item_layout, parent, false);

            TextView trainNum = (TextView) v.findViewById(R.id.trainNum);
            TextView trainDepart = (TextView) v.findViewById(R.id.departTime);
            TextView trainArrive = (TextView) v.findViewById(R.id.arriveTime);

            holder.num = trainNum;
            holder.depart = trainDepart;
            holder.arrive = trainArrive;


            v.setTag(holder);
        }
        else
        {
            holder = (TrainHolder) v.getTag();
        }

        Train t = trainList.get(position);
        holder.num.setText(t.getNum());
        holder.depart.setText(t.getDepart());
        holder.arrive.setText(t.getArrive());

        return v;
    }

}
