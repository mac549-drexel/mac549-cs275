package com.example.mark.septatwo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mark on 3/11/15.
 */
public class TrainListTask extends AsyncTask<Void, Void, Void>
{
    public String getUrl() {
        return url;
    }

    String url;
    String source, dest;
    TrainListAdapter adapter;

    public List<Train> getData() {
        return data;
    }

    List< Train > data;

    public TrainListTask(String _url)
    {
        url = _url;
        data = new ArrayList<Train>();
    }

    @Override
    protected Void doInBackground(Void... params)
    {
        try {
            String nextToArrive = getUrl();
            URL url = new URL(nextToArrive);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(
                    new InputStreamReader(
                            (InputStream) request.getContent()
                    )
            );
            //Log.i("GOOD", "Parsed Json:" + root.toString());
            JsonArray trains = root.getAsJsonArray();
            Train firstTrain = new Train("Train Num", "Depart. Time", "Arriv. Time");
            data.add(firstTrain);
            for (JsonElement train : trains)
            {
                JsonObject currTrain = train.getAsJsonObject();
                String tNum = currTrain.get("orig_train").getAsString();
                String tDep = currTrain.get("orig_departure_time").getAsString();
                String tArr = currTrain.get("orig_arrival_time").getAsString();
                data.add(new Train(tNum, tDep, tArr));
            }
        }
        catch (Exception e)
        {
            Log.e("ERROR", e.getMessage());
        }
        /*try
        {
            Log.i("GOOD", "Got to Try block");
            URL nextToArrive = new URL(getUrl());
            HttpURLConnection request = (HttpURLConnection) nextToArrive.openConnection();
            request.connect();

            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonArray trains = root.getAsJsonArray();

            for (JsonElement train : trains)
            {
                JsonObject currTrain = train.getAsJsonObject();
                String tNum = currTrain.get("orig_train").getAsString();
                String tDep = currTrain.get("orig_departure_time").getAsString();
                String tArr = currTrain.get("orig_arrival_time").getAsString();
                data.add(new Train(tNum, tDep, tArr));
            }
        }
        catch (IOException e)
        {
            Log.e("ERROR", "IOException");
        }*/
        /*JsonArray trains = getTrains();
        for (JsonElement train : trains)
        {
            JsonObject currTrain = train.getAsJsonObject();
            String tNum = currTrain.get("orig_train").getAsString();
            String tDep = currTrain.get("orig_departure_time").getAsString();
            String tArr = currTrain.get("orig_arrival_time").getAsString();
            data.add(new Train(tNum, tDep, tArr));
        }

        //Train t = new Train ("thisisNum", "thisisDep", "thisisArr");
        //data.add(t);*/


        return null;
    }
/*
    @Override
    public void onPostExecute(Void x)
    {

    }


    public JsonArray getTrains()
    {
        final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        final HttpGet getRequest = new HttpGet(url);
        JsonParser jp = new JsonParser();
        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode
                        + " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    inputStream = entity.getContent();
                    JsonElement root = jp.parse(new InputStreamReader(inputStream));
                    return root.getAsJsonArray();
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or
            // IllegalStateException
            getRequest.abort();
            Log.w("ImageDownloader", "Error while retrieving bitmap from " + url);
        } finally {
            if (client != null) {
                client.close();
            }
        }
        return null;
    }*/

}
