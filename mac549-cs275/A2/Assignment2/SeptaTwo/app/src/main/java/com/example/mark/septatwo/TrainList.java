package com.example.mark.septatwo;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class TrainList extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train_list);

        Intent i = getIntent();

        String source = i.getStringExtra("source");
        String dest = i.getStringExtra("dest");
        String nextToArrive = "http://www3.septa.org/hackathon/NextToArrive/"
                + source
                + "/" + dest
                + "/10";
        TrainListTask myTask = new TrainListTask(nextToArrive.replace(" ", "%20"));

        TrainListAdapter myAdapter = new TrainListAdapter(this, myTask.getData());

        ListView l = (ListView) findViewById(R.id.trainList);
        l.setAdapter(myAdapter);
        l.setOnItemLongClickListener(
            new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {

                LinearLayout listItem = (LinearLayout) parent.getChildAt(position);
                GridLayout trainStats = (GridLayout) listItem.getChildAt(0);
                TextView trainNum = (TextView) trainStats.getChildAt(0);
                TrainInfoTask trainInfo = new TrainInfoTask(TrainList.this);
                if (!(trainNum.getText().toString().equals("Train Num")))
                {
                    trainInfo.execute(trainNum.getText().toString());
                }
                //trainInfo.execute(trainNum.getText().toString());


                return true;
            }
        });

        myTask.execute();



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_train_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
