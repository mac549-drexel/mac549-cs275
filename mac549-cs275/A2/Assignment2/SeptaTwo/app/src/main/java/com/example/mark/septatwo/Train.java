package com.example.mark.septatwo;

/**
 * Created by Mark on 3/11/15.
 */
public class Train
{
    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrive() {
        return arrive;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    String num;
    String depart;
    String arrive;

    public Train(String num, String depart, String arrive)
    {
        this.num = num;
        this.depart = depart;
        this.arrive = arrive;
    }

}
