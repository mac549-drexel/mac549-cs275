package com.example.mark.septatwo;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Mark on 3/12/15.
 */
public class TrainInfoTask extends AsyncTask<String, Void, String>
{

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    String info;

    Context cxt;

    public TrainInfoTask(Context _cxt)
    {
        info = "";
        cxt = _cxt;
    }

    protected String doInBackground(String... params)
    {
        String trainNum = params[0];
        String url = "http://www3.septa.org/hackathon/RRSchedules/" + trainNum;
        Log.i("GOOD", "Made it to getTrainInfo()");
        try
        {
            URL rrsSchedule = new URL(url.replace(" ", "%20"));
            Log.i("GOOD", "Made URL: " + rrsSchedule.toString());
            HttpURLConnection connection = (HttpURLConnection) rrsSchedule.openConnection();
            Log.i("GOOD", "Got to openConnection()");
            connection.connect();
            Log.i("GOOD", "Got to connect()");
            Log.i("GOOD", "Got response code: " + connection.getResponseCode());
            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(
                    new InputStreamReader(
                            (InputStream) connection.getContent()
                    ));
            JsonArray stations = root.getAsJsonArray();
            String schdTime = "";
            String actTime = "";
            String stationName = "";
            for (JsonElement station : stations)
            {
                JsonObject currStation = station.getAsJsonObject();
                if (!(currStation.get("act_tm").getAsString().equals("na")))
                {
                    stationName = currStation.get("station").getAsString();
                    schdTime = currStation.get("sched_tm").getAsString();
                    actTime = currStation.get("act_tm").getAsString();
                }
                else
                {
                    if (stationName.length() < 1)
                        return "Train hasn't left yet!";
                    break;
                }
            }
            return info += "Train " + trainNum + " left station: " + stationName
                    + "\nat: " + actTime
                    + "\nscheduled: " + schdTime;
        }
        catch (Exception e)
        {
            Log.e("ERROR", "Exception thrown:" + e.getMessage());
            e.printStackTrace();
            return "Bad Json?";
        }
    }

    @Override
    protected void onPostExecute(String _info)
    {
        Toast.makeText(cxt,
                _info,
                Toast.LENGTH_LONG).show();
    }
}
