package com.mocktracker.service;

import javax.ws.rs.GET;

import com.google.gson.*;

import java.util.*;
import java.sql.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/events")
public class ScheduleService 
{
	public String makeJson()
	{
		/*Event m = new Event("Now", "Here");
		Event n = new Event("Then", "There");
		
		List<Event> events = new ArrayList<Event>();
		events.add(n);
		events.add(m);
		Gson g = new Gson();

		return g.toJson(events);*/
		return "";
	}
	
	@GET
	@Path("/makeevent")
	public String makeEvent(@QueryParam("eventid") int _eventID, @QueryParam("teamp") String _teamP, 
			@QueryParam("teamd") String _teamD, @QueryParam("time") String _time, 
			@QueryParam("rm") String _room, @QueryParam("lat") String _lat, 
			@QueryParam("lon") String _lon, @QueryParam("not") String _not)
	{
		Event event = new Event(_eventID, _teamP, _teamD, _time, _room, _lat, _lon, _not);
		Connection c = null;
		try 
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:schedule.db");
			Statement stmt = c.createStatement();
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Events (ID Integer, TeamP Text, TeamD Text, Time Text, Room Text, Latitude Text, Longitude Text, Notification Text);");
			//stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Events (Time Text, Room Text);");
			stmt.executeUpdate("INSERT INTO Events (ID, TeamP, TeamD, Time, Room, Latitude, Longitude, Notification)" +
			"Values ('" + _eventID + "', '" + _teamP + "', '"+ _teamD 
			+ "', '"+ _time + "', '" + _room + "', '" + _lat +  "', '" + _lon + "', '" + _not + "');");
			
			c.close();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		Gson g = new Gson();
		
		return g.toJson(event);
	}
	
	@GET
	@Path("/termination")
	public void deleteEvents()
	{
		Connection c = null;
		try 
		{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:schedule.db");
			Statement stmt = c.createStatement();
			//stmt.executeUpdate("DROP TABLE Events;");
			stmt.executeUpdate("DELETE FROM Events;");
			
			c.close();
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	@Path("/listall")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getEvents() 
	{
	   Connection c = null;
	   ArrayList<Event> eventList = new ArrayList<Event>();
	   
	   // Query the database
	   try {
	     Class.forName("org.sqlite.JDBC");
	     c = DriverManager.getConnection("jdbc:sqlite:schedule.db");
	     Statement stmt = c.createStatement();
	     ResultSet results = stmt.executeQuery("SELECT * FROM Events;");

	     // iterate over the results
	     while(results.next()) {
	   	 eventList.add(
	   			 new Event(
	   					 results.getInt("ID"), results.getString("TeamP"), results.getString("TeamD"),
	   					 results.getString("Time"), results.getString("Room") ,
	   					 results.getString("Latitude"), results.getString("Longitude"),
	   					 results.getString("Notification")
	   					 )
	   			 );
	     }
	     
	     c.close();
	   } catch ( Exception e ) {
	     System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	   }	
	   
	   // Convert to json
	   Gson g = new Gson();
	   return "" + g.toJson(eventList); 
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String sayHtmlHello() 
	{
	  //return makeJson();
		return "";
	}
	
}
