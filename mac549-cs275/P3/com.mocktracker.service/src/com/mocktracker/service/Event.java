package com.mocktracker.service;

//class to hold specific event data
public class Event {
	  
	  private int eventID;
	  private String teamP;
	  private String teamD;
	  private String time;
	  private String room;
	  private String lat;
	  private String lon;
	  private String not;
	  
	  public void setTime(String _time)
	  {
	    time = _time;
	  }
	  
	  public void setRoom(String _room)
	  {
	    room = _room;
	  }
	  
	  public String getTime() { return time; }
	  
	  public String getRoom() { return room; }
	  
	  //GETTERS AND SETTERS
	  
	  public Event(int _eventID, String _teamP, String _teamD, 
			  String _time, String _room, String _lat, String _lon, String _not )
	  {
	    time = _time;
	    room = _room;
	    eventID = _eventID;
	    teamP = _teamP;
	    teamD = _teamD;
	    lat = _lat;
	    lon = _lon;
	    not = _not;
	  }
	  

}
