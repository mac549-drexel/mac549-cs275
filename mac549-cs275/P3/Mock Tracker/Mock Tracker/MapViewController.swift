//
//  MapViewController.swift
//  Mock Tracker
//
//  Created by Hollis Liu on 3/15/15.
//  Copyright (c) 2015 Drexel University. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate{

    @IBOutlet weak var map: MKMapView!
    
    var username = ""
    var locs = [GPSInfo]()
    var gpsFetcher = Fetcher()
    
    var anns = [MKPointAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        gpsFetcher.fetchEvents(username)
        self.locs = gpsFetcher.locs
        
        //println(self.locs)
        
        // Do any additional setup after loading the view.
        for n in 0..<locs.count{
            var loc = CLLocationCoordinate2DMake((locs[n].lat as NSString).doubleValue,(locs[n].lon as NSString).doubleValue)
            
            var tempAnn = MKPointAnnotation()
            tempAnn.coordinate = loc
            tempAnn.title = locs[n].room
            tempAnn.subtitle = "Don't miss it!"
            
            anns.append(tempAnn)
        }
        
        var wloc = CLLocationCoordinate2DMake(39.9500, -75.190)
        let span = MKCoordinateSpanMake(0.015, 0.015)
        let reg = MKCoordinateRegionMake(wloc, span)
        self.map.region = reg

        
        self.map.addAnnotations(anns)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
