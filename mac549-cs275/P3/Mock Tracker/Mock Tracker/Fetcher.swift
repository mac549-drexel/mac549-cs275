//
//  FetchWeatherInHour.swift
//  Weather
//
//  Created by Hollis Liu on 2/28/15.
//  Copyright (c) 2015 Drexel University. All rights reserved.
//

import Foundation

class Fetcher {
    
    var events = [Event]()
    var nots = [String]()
    var locs = [GPSInfo]()
    
    func fetchEvents(username: String){
        
        //network call
        /*
        var urlString = ""//some url
        
        //replace the space
        urlString = urlString.stringByReplacingOccurrencesOfString(" ", withString: "%20", options: NSStringCompareOptions.LiteralSearch, range: nil)
        //get location through http call
        let lookupUrl = NSURL(string: urlString)
        let jsonData = NSData(contentsOfURL: lookupUrl!)
        let trainJson = JSON(data: jsonData!)
        
        for hr in 0..<trainJson.count{
            //var temp = Train(num: trainJson[hr]["orig_train"].string!, dep: trainJson[hr]["orig_departure_time"].string!, arr: trainJson[hr]["orig_arrival_time"].string!)
            //self.trains.append(temp)
        }
*/
        let path = NSBundle.mainBundle().pathForResource("sampleData", ofType: "json")
        let jsonData = NSData(contentsOfMappedFile: path!)
        let EventJson = JSON(data: jsonData!)
        
        for e in 0..<EventJson.count{
            if ((EventJson[e]["teamP"].string! == username) || (EventJson[e]["teamD"].string! == username)){
                
                var temp = Event(eventID: EventJson[e]["eventID"].int!, teamP: EventJson[e]["teamP"].string!, teamD: EventJson[e]["teamD"].string!, startTime: EventJson[e]["time"].string!, rm: EventJson[e]["room"].string!, lat: EventJson[e]["lat"].string!, lon: EventJson[e]["lon"].string!, notification: EventJson[e]["not"].string!)
                self.events.append(temp)
                if (EventJson[e]["not"].string! !=  "none"){
                    self.nots.append(EventJson[e]["not"].string!)
                }
                var xtemp = GPSInfo(lat: EventJson[e]["lat"].string!, lon: EventJson[e]["lon"].string!, room: EventJson[e]["room"].string!)
                self.locs.append(xtemp)
            }
        }
        
    }
    
}