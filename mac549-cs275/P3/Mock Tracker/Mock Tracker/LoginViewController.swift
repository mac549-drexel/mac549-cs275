//
//  LoginViewController.swift
//  Mock Tracker
//
//  Created by Hollis Liu on 3/15/15.
//  Copyright (c) 2015 Drexel University. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var username: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.username.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        var next = segue.destinationViewController as UITabBarController
        var schedule = next.viewControllers![0] as ScheduleTableViewController
        var not = next.viewControllers![1] as NotificationTableViewController
        var map = next.viewControllers![2] as MapViewController
        
        schedule.username = username.text
        not.username = username.text
        map.username = username.text
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
