//
//  Team.swift
//  Mock Tracker
//
//  Created by Hollis Liu on 3/15/15.
//  Copyright (c) 2015 Drexel University. All rights reserved.
//

import Foundation

class Event{
    
    var eventId: Int
    var teamP: String
    var teamD: String
    var startTime: String
    var rm: String
    var lat: String
    var lon: String
    var notification: String
    
    init(eventID: Int, teamP: String, teamD: String, startTime: String, rm: String, lat: String, lon: String, notification: String){
        
        self.eventId = eventID
        self.teamP = teamP
        self.teamD = teamD
        self.startTime = startTime
        self.rm = rm
        self.lat = lat
        self.lon = lon
        self.notification = notification
    }
    
}