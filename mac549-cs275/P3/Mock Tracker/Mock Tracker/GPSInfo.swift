//
//  GPSInfo.swift
//  Mock Tracker
//
//  Created by Hollis Liu on 3/16/15.
//  Copyright (c) 2015 Drexel University. All rights reserved.
//

import Foundation

class GPSInfo {
    
    var lat: String
    var lon: String
    var room: String
    
    init(lat: String, lon: String, room: String){
        
        self.lat = lat
        self.lon = lon
        self.room = room
    }
}
