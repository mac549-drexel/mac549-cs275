//
//  ScheduleTableViewCell.swift
//  Mock Tracker
//
//  Created by Hollis Liu on 3/16/15.
//  Copyright (c) 2015 Drexel University. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    
    var event: Event?{
        didSet{
            updateUI()
        }
    }

    @IBOutlet weak var p: UILabel!
    @IBOutlet weak var d: UILabel!
    @IBOutlet weak var no: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(){
        
        p.text = nil
        d.text = nil
        no.text = nil
        time.text = nil
        
        if let event = self.event{
            p.text = event.teamP
            d.text = event.teamD
            no.text = "\(event.eventId)"
            time.text = "\(event.startTime):00"//"- \(event.startTime + 4):00"
            
        }
        
        
    }

}
