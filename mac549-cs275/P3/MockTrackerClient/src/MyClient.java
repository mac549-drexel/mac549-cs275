import java.util.*;
import java.io.*;
import java.net.*;

public class MyClient {

	public static void main(String[] args) {
		//Get baseline data from CSV file
		try
		{
			Scanner in = new Scanner(new File("src/test_data.csv"));
			while (in.hasNextLine())
			{
				urlCall(in.nextLine().split(","));
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Can't find the file");
		}
		
	}
	
	public static void urlCall(String[] args)
	{
		//make a url call that will create data
		String url = "";
		if (args.length >= 8)
		{
			url = "http://localhost:8080/com.mocktracker.service/events/makeevent"
					+ "?eventid=" + args[0] + "&teamp=" + args[1] + "&teamd=" + args[2]
					+ "&time=" + args[3] + "&rm=" + args[4] + "&lat=" + args[5]
					+ "&lon=" + args[6] + "&not=" + args[7];
			//System.out.println(url);
		}
		try
		{
			HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
			connection.connect();
			connection.getContent();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

}
