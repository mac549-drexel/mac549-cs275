import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooSession;


public class CalcGrade 
{
	
	public static boolean isPolySyl(String word, String apiKey) throws MalformedURLException
	{
		String syllableURL = "http://api.wordnik.com:80/v4/word.json/" + word + "/hyphenation" +
				"?limit=50&api_key=" + apiKey;
		URL syllReq = new URL(syllableURL);
		try {
			HttpURLConnection syllResp = (HttpURLConnection) syllReq.openConnection();
			syllResp.connect();
			JsonParser jp = new JsonParser();
			JsonElement syllabRoot = jp.parse
					(
					new InputStreamReader
						(
							(InputStream) syllResp.getContent()
						));
			if (syllabRoot.isJsonArray())
			{
				JsonArray syllables = syllabRoot.getAsJsonArray();
				if (syllables.size() > 2) return true;
			}
			syllResp.disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return false;
		}
		return false;
	}
	public static void main(String[] args) throws Exception
	{
		Scanner in = new Scanner(System.in);
		TembooSession session = new TembooSession("markzakaev", "myFirstApp", "6c710778c8f74fc9abb8d10fa978ec38");

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();
		
		//Get inputs
		System.out.println("Please give the app (consumer) key:");
		String consumerKey = in.nextLine();
		System.out.println("Please give the app (consumer) secret:");
		String consumerSecret = in.nextLine();
		
		// Set inputs
		initializeOAuthInputs.set_ConsumerKey(consumerKey);
		initializeOAuthInputs.set_ConsumerSecret(consumerSecret);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String callbackID = initializeOAuthResults.get_CallbackID();
		String oauthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret();
		System.out.println("Please visit this URL");
		System.out.println(initializeOAuthResults.get_AuthorizationURL());
		System.out.println("(hit enter to finalize OAuth)");
		in.nextLine();
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(callbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(oauthTokenSecret);
		finalizeOAuthInputs.set_ConsumerSecret(consumerSecret);
		finalizeOAuthInputs.set_ConsumerKey(consumerKey);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		String accessToken = finalizeOAuthResults.get_AccessToken();
		String accessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		
		System.out.println("Who's Tweets are you looking for?\nPlease enter a screen name (ex. 'filmcrithulk')");
		String screenName = in.nextLine();
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		//parameters
		userTimelineInputs.set_Count(30);		
		userTimelineInputs.set_ScreenName(screenName);
		//credentials
		userTimelineInputs.set_AccessToken(accessToken);
		userTimelineInputs.set_AccessTokenSecret(accessTokenSecret);
		userTimelineInputs.set_ConsumerSecret(consumerSecret);
		userTimelineInputs.set_ConsumerKey(consumerKey);

		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		
		//Parse response
		JsonParser jp = new JsonParser();
		JsonArray tweets = jp.parse(userTimelineResults.get_Response()).getAsJsonArray();
		
		String apiKey = "a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";
		
		int numPolySyl = 0;
		System.out.println("Polysyllabic words:");
		//loop through each 'sentence'
		for (int i = 0; i < tweets.size(); i++)
		{
			//get the text of the tweet
			String sentence = tweets.get(i).getAsJsonObject().get("text").getAsString();
			//get each word
			String[] words = sentence.split(" ");
			//loop through each word checking if it's polysyllabic
			for (int j = 0; j < words.length; j++)
			{
				//get ride of symbols that might mess up the URL call
				String word = URLEncoder.encode(words[j], "utf-8");
				boolean polySyl = isPolySyl(word, apiKey);
				if (polySyl) 
				{
					System.out.println(words[j] + " ");
					numPolySyl++;
				}
			}
		}
		double grade = 1.0430 * Math.sqrt(numPolySyl) + 3.1291;
		System.out.println("Grade level is: " + grade + " (" + (int)grade + ")");
	}

}
