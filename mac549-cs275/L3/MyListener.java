package com.example.mark.tictactoe;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Mark on 2/15/15.
 */
public class MyListener implements View.OnClickListener
{
    boolean turn1;
    Button[] buttons;
    Button[][] buttonGrid;
    TextView win;

    public MyListener(Button[] buttons, TextView winText) {
        turn1 = true;
        this.buttons = buttons;
        buttonGrid = new Button[3][3];

        int z = 0;
        for (int x = 0; x < buttonGrid.length; x++)
        {
            for (int y = 0; y < buttonGrid[x].length; y++) {
                buttonGrid[x][y] = buttons[z];
                z++;
            }
        }
        win = winText;
    }

    public static boolean checkWin(Button[][] grid)
    {
        //i -= 2131230784;//set i to index of buttons
        //check each row
        for (int i = 0; i < grid.length; i++)
        {
            CharSequence middle = grid[i][1].getText();
            if (grid[i][0].getText().equals(middle)
                    && grid[i][2].getText().equals(middle)
                    && middle.length() > 0)
            {
                return true;
            }
        }
        //check each column
        for (int i = 0; i < grid[0].length; i++)
        {
            CharSequence middle = grid[1][i].getText();
            if (grid[0][i].getText().equals(middle)
                    && grid[2][i].getText().equals(middle)
                    && middle.length() > 0)
            {
                return true;
            }
        }
        //check diagonal
        CharSequence center = grid[1][1].getText();
        boolean upperLeft = grid[0][0].getText().equals(center)
                && grid[2][2].getText().equals(center)
                && center.length() > 0;
        boolean upperRight = grid[0][2].getText().equals(center)
                && grid[2][0].getText().equals(center)
                && center.length() > 0;
        if (upperLeft || upperRight )
        {
            return true;
        }

        return false;
    }

    public void onClick(View v)
    {
        CharSequence token;
        if (turn1) token = "X";
        else token = "O";
        CharSequence turnText;
        Button b = (Button) v;
        if (b.getText().length() < 1)
        {
            b.setText(token);
            if (checkWin(buttonGrid))
            {
                CharSequence winnerText;
                if (turn1) { winnerText = "X's Win!"; }
                else { winnerText = "O's Win!"; }
                win.setText(winnerText);
            }
            else
            {
                turn1 = !turn1;
                if (turn1) token = "X";
                else token = "O";
                turnText = token + "'s turn!";
                win.setText(turnText);
            }
        }
        else if (b.getText().equals("Reset"))
        {
            turn1 = true;
            win.setText((CharSequence) "X's turn!");
            for (Button currB : buttons)
            {
                currB.setText((CharSequence) "");
            }
        }

    }
}
