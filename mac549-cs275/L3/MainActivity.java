package com.example.mark.tictactoe;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GridLayout grid = (GridLayout) findViewById(R.id.grid);
        Button[] buttons = new Button[grid.getChildCount()];
        TextView winText = (TextView) findViewById(R.id.winText);

        for (int i = 0; i < buttons.length; i++)
        {
            buttons[i] = (Button) grid.getChildAt(i);
        }
        MyListener l = new MyListener(buttons, winText);
        for (Button currB : buttons)
        {
            currB.setOnClickListener(l);
        }
        Button res = (Button)findViewById(R.id.reset);
        res.setOnClickListener(l);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
