package com.example.billmongan.myapplication;

/**
 * Created by Mark on 3/4/15.
 */
public class Weather
{
    private String time;
    private String description;
    private String humidity;
    private String temperature;
    private String iconURL;

    public Weather(String _time, String _description, String _humidity,
                   String _temperature, String _iconURL)
    {
        time = _time;
        description = _description;
        humidity = _humidity;
        temperature = _temperature;
        iconURL = _iconURL;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

}
