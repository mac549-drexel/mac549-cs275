package com.example.billmongan.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //String from[] = {"time", "description", "humidity", "temperature"};
        //int to[] = {R.id.time, R.id.description, R.id.humidity, R.id.temperature};
        MyBgTask bgTask = new MyBgTask();

        //SimpleAdapter adapter = new SimpleAdapter(this, bgTask.getData(),
        //        R.layout.list_item_layout, from, to);
        MyAdapter adapter = new MyAdapter(bgTask.getData(), this);

        ListView l = (ListView) findViewById(R.id.listView);
        l.setAdapter(adapter);

        bgTask.execute();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
