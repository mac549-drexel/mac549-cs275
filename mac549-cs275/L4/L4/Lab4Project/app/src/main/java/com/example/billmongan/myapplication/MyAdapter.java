package com.example.billmongan.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Mark on 3/4/15.
 */
public class MyAdapter extends ArrayAdapter<Weather>
{

    private Context context;
    private List<Weather> weatherList;

    public MyAdapter(List<Weather> weatherList, Context cxt)
    {
        super(cxt, R.layout.list_item_layout, weatherList);
        this.weatherList = weatherList;
        this.context = cxt;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        WeatherHolder holder = new WeatherHolder();

        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item_layout, parent, false);
            TextView time = (TextView) v.findViewById(R.id.time);
            TextView desc = (TextView) v.findViewById(R.id.description);
            TextView humid = (TextView) v.findViewById(R.id.humidity);
            TextView temp = (TextView) v.findViewById(R.id.temperature);
            ImageView img = (ImageView) v.findViewById(R.id.icon);

            holder.time = time;
            holder.desc = desc;
            holder.humid = humid;
            holder.temp = temp;
            holder.img = img;

            v.setTag(holder);
        }
        else
        {
            holder = (WeatherHolder) v.getTag();
        }

        Weather w = weatherList.get(position);
        holder.time.setText(w.getTime());
        holder.temp.setText(w.getTemperature());
        holder.humid.setText(w.getHumidity());
        holder.desc.setText(w.getDescription());
        new ImageLoaderTask(holder.img).execute(w.getIconURL());

        return v;
    }

    /*@Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getCount()
    {
        return 0;
    }*/

}
