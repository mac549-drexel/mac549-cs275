package com.example.billmongan.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by billmongan on 2/24/15.
 * Modified by Mark Cohen on 3/4/15
 */
public class MyBgTask extends AsyncTask<Void, Void, Void> {

    List< Weather > data;

    public List< Weather > getData() {
        return data;
    }
    //give access to ImageView
    public MyBgTask() {
        data = new
                ArrayList<Weather>();
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Connect to the URL
        try {
            String key = "b980b2496eb2af7c";
            String zip = "19104";
            String sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/"+ zip + ".json";
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootObj = root.getAsJsonObject();
            JsonArray hours = rootObj.getAsJsonArray("hourly_forecast");

            for(int i = 0; i < hours.size(); i++)
            {
                //the current hour
                JsonObject hour = hours.get(i).getAsJsonObject();

                //the time object
                String clock = hour.get("FCTTIME").getAsJsonObject().get("civil").getAsString();
                String dateMonth = hour.get("FCTTIME").getAsJsonObject().get("mon_abbrev").getAsString();
                String dateDay = hour.get("FCTTIME").getAsJsonObject().get("mday").getAsString();
                String time = dateMonth + " " + dateDay + ", " + clock;

                //the description
                String desc = hour.get("condition").getAsString();

                //humidity
                String humid = hour.get("humidity").getAsString() + "%";

                //temperature
                String temp = hour.get("temp").getAsJsonObject().get("english").getAsString() + "F";

                //the image URL
                String imgURL = hour.get("icon_url").getAsString();

                Weather w = new Weather(time, desc, humid, temp, imgURL);
                data.add(w);
                Map<String, String> values = new HashMap<String, String>();
                values.put("time", w.getTime());
                values.put("description", w.getDescription());
                values.put("humidity", w.getHumidity());
                values.put("temperature", w.getTemperature());

                //LoadImage imgTsk = new LoadImage(imgURL, img);
                /*Bitmap bitmap;
                Map<String, Bitmap> imageValue = new HashMap<String, Bitmap>();
                try
                {
                    bitmap = BitmapFactory.decodeStream((InputStream) new URL(imgURL).getContent());
                    imageValue.put("icon", bitmap);
                    //img.setImageBitmap(bitmap);
                }
                catch (Exception e){}*/

                //data.add(values);

            }
            request.disconnect();
        } catch(IOException e) {
            Log.e("ERROR", e.toString());
        }

        return null;
    }

    protected void onPostExecute(Void... params) {

    }
}
