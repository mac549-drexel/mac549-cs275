package com.example.billmongan.myapplication;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Mark on 3/4/15.
 */
public class WeatherHolder
{
    TextView time;
    TextView temp;
    TextView desc;
    TextView humid;
    ImageView img;
}
